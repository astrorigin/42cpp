/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 23:26:04 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/22 12:37:46 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <sstream>

#include "Contact.hpp"
#include "PhoneBook.hpp"

static std::string str_strip(const std::string& inpt)
{
	std::string::const_iterator start_it = inpt.cbegin();
    while (std::isspace(*start_it))
        ++start_it;
	if (start_it == inpt.cend())
		return "";
	std::string::const_reverse_iterator end_it = inpt.crbegin();
    while (std::isspace(*end_it))
        ++end_it;
    return std::string(start_it, end_it.base());
}

static void _printUsage(void)
{
	std::cout << "Phonebook42 usage: EXIT or Ctrl-D | ADD | SEARCH"
		<< std::endl;
}

int main(void)
{
	PhoneBook	bk = PhoneBook();
	std::string	input;

	_printUsage();

	while (!std::cin.eof())
	{
		std::cout << "COMMAND ?\n";
		std::getline(std::cin, input);
		input = str_strip(input);

		if (input == "EXIT")
			break;

		if (input == "ADD")
		{
			std::string fnam;
			std::string lnam;
			std::string nick;
			std::string phon;
			std::string secr;

			for (int i = -1; ++i < 5;)
			{
				if (std::cin.eof())
					break;
				switch (i)
				{
				case 0:
					std::cout << "First name ?\n";
					break;
				case 1:
					std::cout << "Last name ?\n";
					break;
				case 2:
					std::cout << "Nickname ?\n";
					break;
				case 3:
					std::cout << "Phone number ?\n";
					break;
				case 4:
					std::cout << "Darkest secret ?\n";
					break;
				}
				std::getline(std::cin, input);
				input = str_strip(input);
				if (input.empty())
				{
					std::cout << "No empty data, please\n";
					--i;
					continue;
				}

				switch (i)
				{
				case 0:
					fnam = input;
					break;
				case 1:
					lnam = input;
					break;
				case 2:
					nick = input;
					break;
				case 3:
					phon = input;
					break;
				case 4:
					secr = input;
					break;
				}
			}

			Contact c = Contact();
			c.set(Contact::col_first_name, fnam);
			c.set(Contact::col_last_name, lnam);
			c.set(Contact::col_nickname, nick);
			c.set(Contact::col_phone, phon);
			c.set(Contact::col_secret, secr);
			bk.addContact(c);
		}

		else if (input == "SEARCH")
		{
			int numContacts = bk.printList();

			if (numContacts == 0)
				continue;

			while (!std::cin.eof())
			{
				int	contactIdx;

				if (numContacts > 1)
					std::cout << "Index ? [0-" << numContacts - 1 << "]\n";
				else
					std::cout << "Index ? [0]\n";
				std::getline(std::cin, input);
				try
				{
					contactIdx = std::stoi(input);
				}
				catch (std::exception e)
				{
					std::cout << "Expecting an integer...\n";
					continue;
				}
				if (contactIdx < 0 || contactIdx > numContacts - 1)
				{
					std::cout << "Index (" << contactIdx << ") not found\n";
					continue;
				}
				bk.printContact(contactIdx);
				break;
			}
		}

		else if (!std::cin.eof())
			_printUsage();
	}

	return 0;
}
