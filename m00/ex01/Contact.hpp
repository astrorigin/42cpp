/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 23:28:30 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/21 10:23:55 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <string>

class Contact
{
public:

	typedef enum
	{
		col_first_name,
		col_last_name,
		col_nickname,
		col_phone,
		col_secret
	}	column;

	Contact(void);

	Contact(const Contact& other);

	~Contact();

	Contact& operator=(const Contact& other);

	std::string get(column idx) const;

	int set(column idx, const std::string& value);

	bool isValid(void) const;

private:

	std::string	m_data[5];

};

#endif
