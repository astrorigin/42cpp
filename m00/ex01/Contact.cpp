/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 23:28:14 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/21 10:23:55 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"

#include <sstream>

Contact::Contact(void)
{
}

Contact::Contact(const Contact& other)
{
	*this = other;
}

Contact::~Contact()
{
}

Contact& Contact::operator=(const Contact& other)
{
	if (this != &other)
	{
		for (int i = -1; ++i < 5;)
			m_data[i] = other.m_data[i];
	}
	return *this;
}

std::string Contact::get(column idx) const
{
	return m_data[idx];
}

int Contact::set(column idx, const std::string& value)
{
	if (value.empty())
		return 0;
	m_data[idx] = value;
	return 1;
}

bool Contact::isValid(void) const
{
	for (int i = -1; ++i < 5;)
	{
		if (m_data[i].empty())
			return false;
	}
	return true;
}
