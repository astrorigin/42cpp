/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 23:29:40 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/17 23:02:54 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>

#include "Contact.hpp"

class PhoneBook
{
public:

	PhoneBook();

	PhoneBook(const PhoneBook& other);

	~PhoneBook();

	PhoneBook& operator=(const PhoneBook& other);

	void addContact(const Contact& contact);

	int printList(void) const;

	void printContact(int idx) const;

private:

	Contact		m_contacts[8];
	int			m_oldest;

	int getNextContactIndex(void);

	static std::string shortString(const std::string& s);

};

#endif
