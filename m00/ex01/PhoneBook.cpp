/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 23:29:24 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/22 12:42:20 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

#include <iomanip>
#include <iostream>

PhoneBook::PhoneBook()
	:
	m_oldest(-1)
{
}

PhoneBook::PhoneBook(const PhoneBook& other)
{
	*this = other;
}

PhoneBook::~PhoneBook()
{
}

PhoneBook& PhoneBook::operator=(const PhoneBook& other)
{
	if (this != &other)
	{
		m_oldest = other.m_oldest;
		for (int i = -1; ++i < 8;)
		{
			m_contacts[i] = other.m_contacts[i];
		}
	}
	return *this;
}

void PhoneBook::addContact(const Contact& contact)
{
	m_contacts[getNextContactIndex()] = contact;
}

int PhoneBook::printList(void) const
{
	std::cout
		<< "----------+----------+----------+----------+" << std::endl
		<< std::setw(10) << "Index" << std::setw(0) << '|'
		<< std::setw(10) << "First name" << std::setw(0) << '|'
		<< std::setw(10) << "Last name" << std::setw(0) << '|'
		<< std::setw(10) << "Nickname" << std::setw(0) << '|' << std::endl
		<< "----------+----------+----------+----------+" << std::endl;
	for (int i = -1; ++i < 8;)
	{
		if (!m_contacts[i].isValid())
			return i;
		const Contact& c = m_contacts[i];
		std::cout
			<< std::setw(10) << i
			<< std::setw(0) << '|'
			<< std::setw(10) << shortString(c.get(Contact::col_first_name))
			<< std::setw(0) << '|'
			<< std::setw(10) << shortString(c.get(Contact::col_last_name))
			<< std::setw(0) << '|'
			<< std::setw(10) << shortString(c.get(Contact::col_nickname))
			<< std::setw(0) << '|' << std::endl;
		std::cout
			<< "----------+----------+----------+----------+" << std::endl;
	}
	return 8;
}

void PhoneBook::printContact(int idx) const
{
	if (idx < 0 || idx > 7 || !m_contacts[idx].isValid())
	{
		std::cout << "Index (" << idx << ") not found" << std::endl;
		return;
	}
	const Contact& c = m_contacts[idx];
	for (int i = -1; ++i < 5;)
	{
		switch (i)
		{
		case 0:
			std::cout << "First name: " << c.get(Contact::col_first_name);
			break;
		case 1:
			std::cout << "Last name: " << c.get(Contact::col_last_name);
			break;
		case 2:
			std::cout << "Nickname: " << c.get(Contact::col_nickname);
			break;
		case 3:
			std::cout << "Phone number: " << c.get(Contact::col_phone);
			break;
		case 4:
			std::cout << "Darkest secret: " << c.get(Contact::col_secret);
			break;
		}
		std::cout << std::endl;
	}
}

int PhoneBook::getNextContactIndex()
{
	if (m_oldest == 7)
		m_oldest = -1;
	return ++m_oldest;
}

std::string PhoneBook::shortString(const std::string& s)
{
	if (s.length() > 10)
	{
		return s.substr(0, 9) + '.';
	}
	return s;
}
