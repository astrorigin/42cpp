/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 17:46:46 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/20 10:44:08 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <locale>
#include <string>

/**
 *	@brief			Turn string in uppercase.
 *	@param[in] s	Input string.
 *	@return			String in uppercase, according to current locale.
 */
static std::string str_toupper(std::string s)
{
	std::locale loc;

	for (std::string::size_type i = 0; i < s.length(); ++i)
	{
		s[i] = std::toupper(s[i], loc);
	}
	return s;
}

int main(int argc, char *argv[])
{
	if (argc == 1)
	{
		// shout default message
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
		return 0;
	}
	for (int i = 0; ++i < argc;)
	{
		std::cout << str_toupper(argv[i]);
	}
	std::cout << std::endl;	// put a newline and flush the buffer
	return 0;
}
