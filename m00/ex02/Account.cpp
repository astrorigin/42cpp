/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/23 19:39:22 by pmarquis          #+#    #+#             */
/*   Updated: 2023/04/03 12:09:57 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "Account.hpp"

#include <iostream>
#include <ctime>
#include <cstdio>

int Account::getNbAccounts(void)
{
	return _nbAccounts;
}

int Account::getTotalAmount(void)
{
	return _totalAmount;
}

int Account::getNbDeposits(void)
{
	return _totalNbDeposits;
}

int Account::getNbWithdrawals(void)
{
	return _totalNbWithdrawals;
}

Account::Account(void)
	:
	_accountIndex(_nbAccounts++),
	_amount(0),
	_nbDeposits(0),
	_nbWithdrawals(0)
{
	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";amount:" << _amount
		<< ";created\n";
}

Account::Account(int initial)
	:
	_accountIndex(_nbAccounts++),
	_amount(initial),
	_nbDeposits(0),
	_nbWithdrawals(0)
{
	_totalAmount += initial;

	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";amount:" << _amount
		<< ";created\n";
}

Account::~Account(void)
{
	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";amount:" << _amount << ";closed\n";
}

void Account::makeDeposit(int amount)
{
	_amount += amount;
	_nbDeposits += 1;
	_totalAmount += amount;
	_totalNbDeposits += 1;

	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";p_amount:" << _amount - amount
		<< ";deposit:" << amount
		<< ";amount:" << _amount
		<< ";nb_deposits:" << _nbDeposits << "\n";
}

bool Account::makeWithdrawal(int amount)
{
	if (_amount - amount < 0)
	{
		_displayTimestamp();
		std::cout
			<< " index:" << _accountIndex
			<< ";p_amount:" << _amount
			<< ";withdrawal:refused\n";
		return false;
	}
	_amount -= amount;
	_nbWithdrawals += 1;
	_totalAmount -= amount;
	_totalNbWithdrawals += 1;

	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";p_amount:" << _amount + amount
		<< ";withdrawal:" << amount
		<< ";amount:" << _amount
		<< ";nb_withdrawals:" << _nbWithdrawals << "\n";
	return true;
}

int Account::checkAmount(void) const
{
	return _amount;
}

void Account::displayStatus(void) const
{
	_displayTimestamp();
	std::cout
		<< " index:" << _accountIndex
		<< ";amount:" << _amount
		<< ";deposits:" << _nbDeposits
		<< ";withdrawals:" << _nbWithdrawals << "\n";
}

void Account::displayAccountsInfos(void)
{
	_displayTimestamp();
	std::cout
		<< " accounts:" << _nbAccounts
		<< ";total:" << _totalAmount
		<< ";deposits:" << _totalNbDeposits
		<< ";withdrawals:" << _totalNbWithdrawals << "\n";
}

void Account::_displayTimestamp(void)
{
	//std::cout << "[19920104_091532]";
	std::time_t t = std::time(0);
	std::tm* tm = std::localtime(&t);
#if 0
	std::cout
		<< "["
		<< tm->tm_year + 1900
		<< (tm->tm_mon < 10 ? std::string("0") + std::to_string(tm->tm_mon) : std::to_string(tm->tm_mon))
		<< (tm->tm_mday < 10 ? std::string("0") + std::to_string(tm->tm_mday) : std::to_string(tm->tm_mday))
		<< "_"
		<< (tm->tm_hour < 10 ? std::string("0") + std::to_string(tm->tm_hour) : std::to_string(tm->tm_hour))
		<< (tm->tm_min < 10 ? std::string("0") + std::to_string(tm->tm_min) : std::to_string(tm->tm_min))
		<< (tm->tm_sec < 10 ? std::string("0") + std::to_string(tm->tm_sec) : std::to_string(tm->tm_sec))
		<< "]";
#else
	char buf[32];
	std::sprintf(buf, "[%04d%02d%02d_%02d%02d%02d]",
		tm->tm_year + 1900, tm->tm_mon, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	std::cout << buf;
#endif
}

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;

